# README #

This Read me will expain the test analysis and test design technique followed for software testing of cafe town send portal. 
It also includes the test automation framework followed along with it's set up details. It also has differnet attachments which can give you more details than below.


### What is this repository for? ###
TownSend portal has its own login credentials, after logging in, it will show the list of employees already present in the system. And the portal contains the facility to Create, Edit and Delete employees. This repository contains the web automation tests which will test the TownSend portal.

### Technologies used ###
* Cucumber
* Serenity 
* Selenium
* Java v1.8
* Maven 
* Junit Framework

### How to understand the test analysis and design ? ###
###Test Analysis And Design:###
In order to analyse different tests to be done for Cafe Town Send Portal, we need to understand the funcational requirements given for this portal. These funcational flow requirements are represented in the form of activity flow. The user can take any decision during flow, System should be able to react and cope up with changing decisions of the user.
Thus decision tables are derived using the activity flow. And these decision tables, gives us different possible test combinations. Using these test combinations, all the possible logical testscenarios and testcases are derived. And Thus tracebility matrix has been created with test scenarios, test cases along with expected result.

###Test Automation:###
These scenarios, inturn has multiple differnet test cases where in user might give different inputs(valid or invalid). Thus the most used test scenarios are been chosen for automation so that ROI will be good enough compared to maual testing. Besides, this automation set up can also be set up in continuous integration platform, so that application healthyness can be assessed in quicker way amidst chaning requirements for this portal.

###References:###
* For better understanding of "Test Analysis", "Test Scenarios" and "Requirements", Refer to Excel sheet "TestAnalysis_CafeTownSendPortalV1.0" in the location mentioned below.
* Test Analysis Method : Decsion Table Mapping, traceability matrix.
* Requirements: Activity Flow
* Location: src/test/resources/testanalysis

### Project Setup:###

Project contains the below set of folders:

 `src/test/java` 
 	
     Packages
     - com.cafetownsend.webautomation.common          (Package with common methods which are reused across the project)
     - com.cafetownsend.webautomation.pageActions     (Package with all the actions that are performed in a page.)
	 - com.cafetownsend.webautomation.pageObjects     (Pacakge with necessary elements located for individual pages, 
	 											       which can be reused directly in the tests.)	
     - com.cafetownsend.webautomation.runner          (Package with the main runner file, which needs to be exeucted to run the whole test)
     - com.cafetownsend.webautomation.stepDefinitions (Package where the actual test steps are implemented)   
     
`src/test/resources`
	
	Sub-Folders
	- drivers  (Folder with ChromeDriver - it can be used to place the driver of the choice)
	- features (Feature files created with Gherkin language using cucumber)
	- testdata (File with input data like url,credentials and employee details)

 `pom.xml`
 
	- It contains external dependencies and plugins of serenity which are needed to run/build the project. 
	  Using this file, complete autmation tests can be run.
	
  `serenity.proeprties` 
 
    - It contains all the external properties such as browser, implicittimeouts, decision about 
	  when to take screenshots, Details needed in the report.
	
###Note: 
  
   - "target" folder has been committed to demonstrate sample execution report with our having to run the complete test suite. 
      If you can successfully run the tests, execution report will automatically be generated.
	
### Test Reports Location after the test run completion :`###
	- target/site/serenity/index.html.

### Automation Test details :###

This automation test suite will use Features files where test scenarios are described and will execute the tests, will generate a test report along with screenshots of the test.
Besides screenshots, test report gives other details such as "time taken to run each test", "overall test results", "features and it's corresponding tests with screenshots"..etc.
In this project, chorme browser has been used for testing the application. we can configure browser of our choice(in "serenity.properties") and corresponding driver, and thus the test can run independent of 
the platform.
	
### Required Software/tools: ###
* Java 8
* Any IDE like Eclipse, IntelliJ..etc
* Maven

### How to execute this project :###

Steps to be followed in order to run this test suite:
	
	Step 1: Clone this repository using SourceTree(or any other tool) and get the project setup on to your workspace.
	Step 2: Use IDE to open the project and build the project.
	Step 3: Open pom.xml file in IDE, and run as "mvn test".
	Step 4: At the end of test execution, execution report can be found in 'target/site/serenity/index.html'.									


### Who do I talk to? ###
Please reach out to me at prabhukiran.narava@gmail.com in case of questions and clarifications related to above set up.