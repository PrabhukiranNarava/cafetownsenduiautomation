Feature: As a vald user, he/she should be able to login successfuly  
  
  Scenario Outline: Validate login functionality 
  	 Given TownSend login page is opened 
     When User name is entered as "<userName>" 
      And Password is entered as "<password>" 
     Then On clicking Login Button, Login should "<result>" successful
    Examples: below is the test data 
      | userName     | password     | result | 
      | Luke         | Skywalker    | Success| 
      | Test         | pswpsw       | Fail   | 
      |              | Password     | Fail   | 
      | Username     |              | Fail   |  