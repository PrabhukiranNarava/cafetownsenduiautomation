Feature: As a valid user, he/she should be able to edit,delete employee details usecase
          
  Scenario: Verify edit employee details funcationality
  	 Given A logged-in admin and atleast one employee details on town send portal 
     When existing employee is edited
     Then the details of that employee should be updated
  
  Scenario: Verify delete employee functionality
  	 Given A logged-in admin and atleast one employee details on town send portal
     When existing employee is deleted
     Then deletion should be successful and employee list page is shown
  