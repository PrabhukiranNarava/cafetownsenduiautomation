/*
 * @Author Prabhukiran Narava
 * This class will read the test data from an excel  sheet 2003-07.
 * It contains the generic methods to read test data using key, value pair.
 */
package com.cafetownsend.webautomation.common;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import jxl.Workbook;
import jxl.read.biff.BiffException;
public class TestDataReader {

	private Map<String, String> map = new HashMap<>();
    File srcPath = new File("src/test/resources/testdata/LoginTestData.xls");
	
	public String readURL() {
		map= getTestData("LoginDetails");
		return map.get("Url");
	}
	
	public String readID() {
		map= getTestData("LoginDetails");
		return map.get("Id");
	}
	
	public String readPassword() {
		map= getTestData("LoginDetails");
		return map.get("Pwd");
	}
	
	public String getEmployeeFirstName(){
		map= getTestData("EmployeeDetails");
		return map.get("EmployeeFirstName");
	}
	
	public String getEmployeeLastName(){
		map= getTestData("EmployeeDetails");
		return map.get("EmployeeLastName");
	}
	
	public String getEmployeeStartDate(){
		map= getTestData("EmployeeDetails");
		return map.get("EmployeeStartDate");
	}
	
	public String getEmployeeEmailId() {
		map= getTestData("EmployeeDetails");
		return map.get("EmployeeEmailId");
	}
	
	public String getEmployeeUpdatedEmailId(){
		map= getTestData("EmployeeDetails");
		return map.get("EmployeeUpdatedEmailId");
	}
	
	
	 /**
     * Creates the test data map reading key, value pairs of test data.
	 * 
	 */
    public Map<String, String> getTestData(String sheetName) {
		Workbook wb = null;
		try {
			wb = Workbook.getWorkbook(srcPath);
		} catch (BiffException | IOException e) {
			e.printStackTrace();
		}
		int numberOfPhysicalColumns = wb.getSheet(sheetName).getRow(0).length;
        for (int i = 0; i < numberOfPhysicalColumns ; i++) {
            map.put(wb.getSheet(sheetName).getCell(i, 0).getContents(), wb.getSheet(sheetName).getCell(i, 1).getContents());
        }
        return map;
    }
		
}
