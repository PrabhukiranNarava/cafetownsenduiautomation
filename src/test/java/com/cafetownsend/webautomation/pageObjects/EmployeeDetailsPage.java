/*
 * @Author Prabhukiran Narava
 * This class is the page object representation of EmployeeDetails page. And it contains all locators and the reusable methods.
 */
package com.cafetownsend.webautomation.pageObjects;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.WebElement;

import com.cafetownsend.webautomation.common.TestDataReader;

public class EmployeeDetailsPage extends PageObject {
	
	
	@FindBy(id= "employee-list")
    WebElementFacade emplist;
	@FindBy(id = "employee-list-container")
	WebElementFacade emplistcontainer;
 	@FindBy(id= "bAdd")
 	WebElementFacade createemployeebutton;
 	@FindBy(id= "bEdit")
 	WebElementFacade editemployeebutton;
 	@FindBy(id= "bDelete")
 	WebElementFacade deleteemployeebutton;
 	
 	/*Method to click create button if it exists */
 	public void clickCreate() {
		if(createemployeebutton.isPresent()){
			createemployeebutton.waitUntilClickable();
			createemployeebutton.click();
		}
	}
 	
 	/*Method to click Edit button if it exists */
 	public void clickEdit() {
 		if(editemployeebutton.isPresent()){
 			editemployeebutton.waitUntilClickable();
 			editemployeebutton.click();
		}
 	}
 	/*Method to click Delete button if it exists */
 	public void clickDelete() {
 		if(deleteemployeebutton.isPresent()){
 			deleteemployeebutton.waitUntilClickable();
 			deleteemployeebutton.click();
		}
 	}
 	/*Method to assert employee name in a employee list */
	public boolean validateEmployeeDetails(String lastname) {
	    if(emplist.isPresent()) {
		return emplist.containsText(lastname);
	    }
	    else {
	        return false;
	    }
	}
	/*Method to validate employee name in a employee list and click Edit */
	public boolean validateEmployeeDetailsAndClickEdit()  {
		TestDataReader testdata = new TestDataReader();
		if(prepareWebElementWithDynamicXpath("//*[@id='employee-list']//li[contains(text(), 'xxxxx')]", testdata.getEmployeeLastName()).isDisplayed()) {
		prepareWebElementWithDynamicXpath("//*[@id='employee-list']//li[contains(text(), 'xxxxx')]", testdata.getEmployeeLastName()).click();
		clickEdit();
		return true;
		}
		return false;
	}
	/*Method to validate employee name in a employee list and click Delete */
	public boolean validateEmployeeDetailsAndClickDelete() {
        TestDataReader testdata = new TestDataReader();
        if(prepareWebElementWithDynamicXpath("//*[@id='employee-list']//li[contains(text(), 'xxxxx')]", testdata.getEmployeeLastName()).isDisplayed()) {
        prepareWebElementWithDynamicXpath("//*[@id='employee-list']//li[contains(text(), 'xxxxx')]", testdata.getEmployeeLastName()).click();
        clickDelete();
        return true;
        }
        return false;
    }
	/*Method to validate employee list*/
	public boolean validateLogin() {
		if(emplistcontainer.isPresent()){
			return true;
		}
		else {
			return false;
		}
	}
	/*Method to find webelement by dynamic xpath*/
	private WebElement prepareWebElementWithDynamicXpath (String xpathValue, String substitutionValue ) {
        return emplist.findElement(By.xpath(xpathValue.replace("xxxxx", substitutionValue)));
	}
}
