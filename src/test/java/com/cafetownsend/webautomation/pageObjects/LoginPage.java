/*
 * @Author Prabhukiran Narava
 * This class is the page object representation of login page. And it contains all locators and the reusable methods for further use.
 */
package com.cafetownsend.webautomation.pageObjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://cafetownsend-angular-rails.herokuapp.com/")
public class LoginPage extends PageObject {

		@FindBy(id = "login-form")
		WebElementFacade loginform;
		
	    @FindBy(xpath = "//input[contains(@type, 'text')]")
	    WebElementFacade usernameField;
	    
	    @FindBy(xpath = "//input[contains(@type, 'password')]")
	    WebElementFacade passwordField;
	    
	    @FindBy(xpath = "//button[contains(@type, 'submit')]" )
	    WebElementFacade LoginBtn;
	    /*Method to type the user name in it's web element*/
	    public void setUsername(String username) {
	    	if(loginform.isPresent()){
	        typeInto(usernameField, username);
	    	}
	     }
	    /*Method to type the password in it's web element*/
	    public void setPassword(String password) {
	    	if(loginform.isPresent()){
	        typeInto(passwordField, password);
	    	}
	    }
	    /*Method to click the login button */
	    public void clickLoginButton() {
	    	if(LoginBtn.isPresent()){
	    	LoginBtn.waitUntilClickable();	
	    	LoginBtn.click();
	    	}
	    }
}

