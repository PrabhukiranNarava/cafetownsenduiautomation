
/*
 * @Author Prabhukiran Narava
 * This class is the page object representation of CreateEmployee page. And it contains all locators and the reusable methods for further use.
 */
package com.cafetownsend.webautomation.pageObjects;

import com.cafetownsend.webautomation.common.TestDataReader;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class CreateEmployeePage extends PageObject {
	 	
	 	@FindBy(xpath= "//input[contains(@ng-model, 'selectedEmployee.firstName')]")
	 	WebElementFacade firstname;
	 	
	 	@FindBy(xpath= "//input[contains(@ng-model, 'selectedEmployee.lastName')]")
	 	WebElementFacade lastname;
	 	
	 	@FindBy(xpath= "//input[contains(@ng-model, 'selectedEmployee.startDate')]")
	 	WebElementFacade startdate;
	 	
	 	@FindBy(xpath= "//input[contains(@ng-model, 'selectedEmployee.email')]")
	 	WebElementFacade email;
	 	
	 	@FindBy(xpath=  "//button[contains(text(), 'Add')]")
	    WebElementFacade addbtn;
		
	 	@FindBy(xpath=  "//button[contains(text(), 'Update')]")
	    WebElementFacade updatebtn;
		/*Method to fill in first Name*/
		public void fillInFirstName(String fname) {
		    if(firstname.isPresent()) 
			firstname.type(fname);
		}
		/*Method to fill in Last name*/
		public void fillInLastName(String lname) {
		    if(lastname.isPresent())
			lastname.type(lname);
		}
		/*Method to fill in startdate*/
		public void fillInStartDate(String sdate) {
		    if(startdate.isPresent())
			startdate.type(sdate);
		}
		/*Method to fill in email id */
		public void fillInEmail(String emailid) {
		    if(email.isPresent())
			email.type(emailid);
		}
		/*Method to click add button*/
		public void cliclAddButton() {
			addbtn.waitUntilClickable();
			addbtn.click();
		}
		/*Method to click update button*/
		public void clickUpdateButton() {
			updatebtn.waitUntilClickable();
			updatebtn.click();
		}
		/*Method to fill all employee details*/
		public void fillInEmployeeDetails() {
			TestDataReader testdata = new TestDataReader();
			fillInFirstName(testdata.getEmployeeFirstName());
			fillInLastName(testdata.getEmployeeLastName());
			fillInStartDate(testdata.getEmployeeStartDate());
			fillInEmail(testdata.getEmployeeEmailId());
		}
		
}
