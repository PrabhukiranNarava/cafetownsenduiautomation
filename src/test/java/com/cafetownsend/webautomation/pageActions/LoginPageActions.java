/*
 * @Author Prabhukiran Narava
 * This class is to represent LoginPage related user actions and its sequence. This class uses the page object representation to validate the behavior.
 */
package com.cafetownsend.webautomation.pageActions;

import net.thucydides.core.annotations.Step;

import static org.junit.Assert.*;

import com.cafetownsend.webautomation.common.TestDataReader;
import com.cafetownsend.webautomation.pageObjects.EmployeeDetailsPage;
import com.cafetownsend.webautomation.pageObjects.LoginPage;

public class LoginPageActions {
	private LoginPage loginpage;
	private EmployeeDetailsPage viewemployeespage;
	
	 @Step
	    public void launch() {
		 loginpage.open();
		 loginpage.getDriver().manage().window().maximize();
	    }

	 @Step
	 	public void fillUserName(String username) {
		 loginpage.setUsername(username);
	 }
	 
	 @Step
	 	public void login() {
		 TestDataReader testdata = new TestDataReader();
		 loginpage.setUsername(testdata.readID());
		 loginpage.setPassword(testdata.readPassword());
		 loginpage.clickLoginButton();
	 }
	 
	 @Step
	 	public void fillPassword(String password) {
		 loginpage.setPassword(password);
	 }
	 
	 @Step
	 	public void validateLogin(String result) {
		 loginpage.clickLoginButton();
		 if(result.equalsIgnoreCase("Success"))
		 {
			 assertEquals(true, viewemployeespage.validateLogin());
		 }
		 else {
			 assertEquals(false, viewemployeespage.validateLogin());
		 }
	 }	
}
