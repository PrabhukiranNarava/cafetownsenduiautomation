/*
 * @Author Prabhukiran Narava
 * This class is to represent all CreateEmployeePage actions and its sequence. This class uses the page object representation to validate the behavior.
 */
package com.cafetownsend.webautomation.pageActions;

import com.cafetownsend.webautomation.common.TestDataReader;
import com.cafetownsend.webautomation.pageObjects.CreateEmployeePage;
import com.cafetownsend.webautomation.pageObjects.EmployeeDetailsPage;

import net.thucydides.core.annotations.Step;

public class CreateEmployeePageActions {

    private CreateEmployeePage employeepage;
	private EmployeeDetailsPage employeedetailspage;
	
	 @Step
	 public void clickCreate() {
		 employeedetailspage.clickCreate();
	 }
	 
	 @Step
	 public void clickUpdate() {
		 employeepage.clickUpdateButton();
	 }
	 
	 @Step
	 public void fillFirstName(String firstname) {
		 employeepage.fillInFirstName(firstname);
	 }
	 @Step
	 public void fillLastName(String lastname) {
		 employeepage.fillInLastName(lastname);
	 }
	
	 @Step
	 public void fillStartDate(String startdate) {
		 employeepage.fillInStartDate(startdate);
	 }
	 @Step 
	 public void fillEmail(String email) {
		 employeepage.fillInEmail(email);
	 }
	 
	 @Step 
     public void fillEmail(){
	     TestDataReader testdata = new TestDataReader();
         employeepage.fillInEmail(testdata.getEmployeeUpdatedEmailId());
     }
	 
	 @Step
	 public void addEmployee()  {
		 clickCreate();
		 employeepage.fillInEmployeeDetails();
		 employeepage.cliclAddButton();
	 }
}
