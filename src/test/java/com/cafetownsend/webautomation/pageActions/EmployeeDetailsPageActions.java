package com.cafetownsend.webautomation.pageActions;

import static org.junit.Assert.assertEquals;

import com.cafetownsend.webautomation.pageObjects.EmployeeDetailsPage;

import net.thucydides.core.annotations.Step;

/*
 * @Author Prabhukiran Narava
 * This class is to represent EmployeeDetailsPage related user actions and its sequence. This class uses the page object representation to validate the behavior.
 */
public class EmployeeDetailsPageActions {

	private EmployeeDetailsPage employeedetailspage;
	
	 @Step
	 public void validateEmployeeDetails(String lastname) {
		 assertEquals(true, employeedetailspage.validateEmployeeDetails(lastname));
	 }
	 @Step
     public void validateEmployeeDetails() {
         assertEquals(true, employeedetailspage.validateLogin());
     }
	
	 @Step
	 public void validateLogin(String result) {
		 if(result.equalsIgnoreCase("be")) {
		 assertEquals(true, employeedetailspage.validateLogin());
		 }
		 else {
			 assertEquals(true, employeedetailspage.validateLogin());
		 }
	 }
	 @Step
	 public void editEmployeeDetails() {
		 assertEquals(true, employeedetailspage.validateEmployeeDetailsAndClickEdit());
	 }
	 
	 @Step
	 public void deleteEmployeeDetails(){
	     assertEquals(true, employeedetailspage.validateEmployeeDetailsAndClickDelete());
	     employeedetailspage.getAlert().accept();
    	 }
	
}