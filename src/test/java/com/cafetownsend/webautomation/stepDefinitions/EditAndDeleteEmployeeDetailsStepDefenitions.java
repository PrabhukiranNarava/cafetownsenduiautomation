/*
 * @Author Prabhukiran Narava
 * This class will contains the step definitions related to Edit and Delete operations.
 * 
 */
package com.cafetownsend.webautomation.stepDefinitions;

import com.cafetownsend.webautomation.pageActions.CreateEmployeePageActions;
import com.cafetownsend.webautomation.pageActions.EmployeeDetailsPageActions;
import com.cafetownsend.webautomation.pageActions.LoginPageActions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class EditAndDeleteEmployeeDetailsStepDefenitions {
	
	@Steps
	LoginPageActions loginstep;
	@Steps
	EmployeeDetailsPageActions employeedetailsstep;
	@Steps
	CreateEmployeePageActions employeepagestep;
	
	@Given("^A logged-in admin and atleast one employee details on town send portal$")
	public void a_logged_in_admin_and_atleast_one_employee_details_on_town_send_portal()  throws Throwable {
		loginstep.launch();
	    loginstep.login();
	    employeepagestep.addEmployee();
	}
	@When("^existing employee is edited$")
	public void existing_employee_is_edited() throws Throwable {
		employeedetailsstep.editEmployeeDetails();
	}

	@Then("^the details of that employee should be updated$")
	public void the_details_of_that_employee_should_be_updated() throws Throwable {
	    employeepagestep.fillEmail();
	    employeepagestep.clickUpdate();
	}

	@When("^existing employee is deleted$")
	public void existing_employee_is_deleted() throws Throwable {
		employeedetailsstep.deleteEmployeeDetails();
	}

	@Then("^deletion should be successful and employee list page is shown$")
	public void deletion_should_be_successful_and_employee_list_page_is_shown() throws Throwable {
		employeedetailsstep.validateEmployeeDetails();
	}
}
