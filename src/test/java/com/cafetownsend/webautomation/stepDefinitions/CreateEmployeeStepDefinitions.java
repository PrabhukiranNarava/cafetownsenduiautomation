
/*
 * @Author Prabhukiran Narava
 * This class contains all the step definitions related to create Employee Page.
 * 
 */
package com.cafetownsend.webautomation.stepDefinitions;

import com.cafetownsend.webautomation.pageActions.CreateEmployeePageActions;
import com.cafetownsend.webautomation.pageActions.LoginPageActions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class CreateEmployeeStepDefinitions {
	@Steps
	LoginPageActions loginstep;
	@Steps
	CreateEmployeePageActions employeestep;
	@Given("^TownSend portal is logged and create employee page is open$")
	public void townsend_portal_is_logged_and_create_employee_page_is_open() throws Throwable {
	    loginstep.launch();
	    loginstep.login();
	    employeestep.clickCreate();
	    
	}

	@When("^First Name is entered as \"([^\"]*)\"$")
	public void first_Name_is_entered_as(String firstname) throws Throwable {
		employeestep.fillFirstName(firstname);
	}

	@When("^Last Name is entered as \"([^\"]*)\"$")
	public void last_Name_is_entered_as(String lastname) throws Throwable {
		employeestep.fillLastName(lastname);
	}

	@When("^Start Date is entered as \"([^\"]*)\"$")
	public void start_Date_is_entered_as(String startdate) throws Throwable {
		employeestep.fillStartDate(startdate);
	}

	@When("^Email Id is entered as \"([^\"]*)\"$")
	public void email_Id_is_entered_as(String emailid) throws Throwable {
		employeestep.fillEmail(emailid);
	}

	@Then("^Create employee should \"([^\"]*)\" successful$")
	public void create_employee_should_successful(String arg1) throws Throwable {
		
	}
	
}
