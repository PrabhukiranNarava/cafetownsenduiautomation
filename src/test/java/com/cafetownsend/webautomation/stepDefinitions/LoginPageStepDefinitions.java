/*
 * @Author Prabhukiran Narava
 * This class contains the step definitions for login page such as filling user name, password and clicking login.
 */
package com.cafetownsend.webautomation.stepDefinitions;

import com.cafetownsend.webautomation.pageActions.LoginPageActions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
public class LoginPageStepDefinitions {
	
	@Steps
	LoginPageActions loginstep;
	
	@Given("^TownSend login page is opened$")
	public void townsend_login_page_is_opened() {
		loginstep.launch();
	}

	@When("^User name is entered as \"([^\"]*)\"$")
	public void user_name_is_entered_as(String username) {
		loginstep.fillUserName(username);
	}

	@When("^Password is entered as \"([^\"]*)\"$")
	public void password_is_entered_as(String password){
		loginstep.fillPassword(password);
	}

	@Then("^On clicking Login Button, Login should \"([^\"]*)\" successful$")
	public void On_clicking_Login_Button_login_should_successful(String result) {
		loginstep.validateLogin(result);
	}

}
