/*
 * @author Prabhukiran Narava
 * 
 * Below annotations namely @Runwith and @CucumberOptions will trigger the entire test run.
 * 
 * 1. @Runwith will trigger the test run with cucumber using serenity.
 * 2. @CucumberOptions will take the features from location for test run.
 * 
 */
package com.cafetownsend.webautomation.runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features= "src/test/resources/features/", glue = "com.cafetownsend.webautomation.stepDefinitions") 
public class RunFeaturesTest {
	
} 