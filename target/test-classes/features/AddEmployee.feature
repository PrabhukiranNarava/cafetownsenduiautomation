Feature: As a valid user, he/she should be able to add an employee. 
  Scenario Outline: Validate create employee page by giving different inputs
    Given TownSend portal is logged and create employee page is open
     When First Name is entered as "<FirstName>" 
      And Last Name is entered as "<LastName>" 
      And Start Date is entered as "<StartDate>" 
      And Email Id is entered as "<EmailId>" 
     Then Create employee should "<CreateResult>" successful
    Examples: below is the test data 
      | FirstName | LastName | StartDate  | EmailId                  | CreateResult | 
      | Iron      | Man      | 2017-10-01 | iron.man@gmail.com       | Be           | 
      |           | Man      | 2017-10-01 | iron.man@gmail.com       | Not be       | 
      | Iron      |          | 2017-10-01 | iron.man@gmail.com       | Not be       | 
      | Iron      | man      | 2017/10/01 | iron.man@gmail.com       | Not be       | 
      | Iron      | Man      | 2017-10-01 | bahubali			     | Not be       | 
      | Iron      | Man      | 2017-10-01 |                          | Not be       | 
  